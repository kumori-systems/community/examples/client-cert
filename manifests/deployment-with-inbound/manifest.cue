package deployment
// If a package name other than "deployment" is used, then the
// "kumorictl register deployment" operation must include the "--package" flag.

import (
  s ".../service-with-inbound:service"
)

#Deployment: { // MANDATORY name
  name: "clientcertdep"
  artifact: s.#Artifact
  config: {
    parameter: {
      certrequired: false
    }
    resource: {
      servercert: certificate: "cluster.core/wildcard-test-kumori-cloud" 
      serverdomain: domain: "clientcertdomain" 
      clientcertca: ca: "clientcertca" 
    }
    scale: detail: frontend: hsize: 1
    resilience: 0
  }
}
