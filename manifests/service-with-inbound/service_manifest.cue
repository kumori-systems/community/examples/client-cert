package service

import (
  k "kumori.systems/kumori:kumori"
  i "kumori.systems/builtins/inbound:service"
  f ".../frontend:component"
)

#Artifact: {
  ref: name:  "service-with-inbound"

  description: {

    config: {
      parameter: {
        certrequired: bool
      }
      resource: {
        servercert: k.#Certificate
        serverdomain: k.#Domain
        clientcertca: k.#CA
      }
    }

    role: {
      frontend: {
        artifact: f.#Artifact
        config: {
          parameter: {}
          resource: {}
          resilience: description.config.resilience
        }
      }
      httpinbound: {
        artifact: i.#Artifact
        config: {
          parameter: {
            type: "https"
            websocket: true
            clientcert: true
            certrequired: description.config.parameter.certrequired
          }
          resource: {
            servercert: description.config.resource.servercert
            serverdomain: description.config.resource.serverdomain
            clientcertca:  description.config.resource.clientcertca
          }
          resilience: description.config.resilience
        }
      }
    }

    srv: {} // No service channels: the inbound is included as part of the service

    connect: {
      // inbound => frontend
      cinbound: {
        as: "lb"
        from: httpinbound: "inbound"
        to: frontend: entrypoint: _
      }
    }
  }
}
