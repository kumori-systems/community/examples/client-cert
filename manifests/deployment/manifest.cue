package deployment
// If a package name other than "deployment" is used, then the
// "kumorictl register deployment" operation must include the "--package" flag.

import (
  s ".../service:service"
)

#Deployment: { // MANDATORY name
  name: "clientcertdep"
  artifact: s.#Artifact
  config: {
    parameter: {}
    resource: {}
    scale: detail: frontend: hsize: 1
    resilience: 0
  }
}
