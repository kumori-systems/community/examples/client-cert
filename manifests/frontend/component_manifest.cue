package component

#Artifact: {
  ref: name:  "frontend"

  description: {

    srv: {
      server: {
        entrypoint: { protocol: "http", port: 8080 }
      }
    }

    config: {
      parameter: {}
      resource: {}
    }

    size: {
      bandwidth: { size: 15, unit: "M" }
    }

    probe: frontend: {
      liveness: {
        protocol: http : { port: srv.server.entrypoint.port, path: "/health" }
        startupGraceWindow: { unit: "ms", duration: 20000, probe: true }
        frequency: "medium"
        timeout: 30000  // msec
      }
      readiness: {
        protocol: http : { port: srv.server.entrypoint.port, path: "/health" }
        frequency: "medium"
        timeout: 30000 // msec
      }
    }

    code: {
      frontend: {
        name: "frontend"
        image: {
          hub: {name: "", secret: "" }
          tag: "kumoripublic/examples-client-cert-frontend:v1.0.7"
        }
        mapping: {
          filesystem: {}
          env: SERVER_PORT_ENV: value: "\(srv.server.entrypoint.port)"
        }
        size: {
          memory: { size: 100, unit: "M" }
          mincpu: 100
          cpu: { size: 200, unit: "m" }
        }
      }
    }
  }
}
