#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
DEFAULTCLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"
CUSTOMSERVERCERT="clientcert-${CLUSTERNAME}-certificate"

# Service variables
INBOUNDNAME="clientcertinb"
DEPLOYNAME="clientcertdep"
DOMAIN="clientcertdomain"
SERVICEURL="clientcert-${CLUSTERNAME}.${REFERENCEDOMAIN}"
CA="clientcertca"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"
KUMORI_INBOUND_VERSION="1.3.0"

# Change if special binaries want to be used
KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

# $1: file base name (for example, "myclientcert")
# $2: company base name (for example, "MyCompany")
# $3: suffix (for example, "1")
# $4: domain
# $5: ca directory (for example, "./cert/ca")
# $6: client cert directory (for example, "./cert/clientcert")
function generateClientCert() {
  mkdir -p ${6};
  openssl genrsa -out ${6}/${1}-${3}.key 2048
  openssl req -new \
    -key ${6}/${1}-${3}.key \
    -out ${6}/${1}-${3}.csr \
    -subj "/C=ES/O=${2}-${3}/CN=${4}"
  openssl x509 -req \
    -days 30 \
    -in ${6}/${1}-${3}.csr \
    -CA ${5}/ca.pem \
    -CAkey ${5}/ca.key \
    -CAcreateserial \
    -out ${6}/${1}-${3}.pem
}

# $1: CA directory (for example, "./cert/ca1")
# $2; Organization issuer base name (for example, "kumori")
# $3: suffix (for example, "1")
function generateCA() {
  mkdir -p ${1}; rm -rf ${1}/*;
  openssl req -x509 \
    -days 3650 \
    -newkey rsa:2018 \
    -nodes \
    -out ${1}/ca.pem \
    -keyout ${1}/ca.key \
    -subj "/C=ES/O=${2}-${3}/CN=${2}-${3}-CN"
}

case $1 in

# ------------------------------------------------------------------------------

# Generates three CA: ca1, ca2, ca3
# Prepare a CA concatenating ca1 and ca2, but NOT ca3
'generate-cas')
  generateCA ./cert/ca1 Kumori 1
  generateCA ./cert/ca2 Kumori 2
  generateCA ./cert/ca3 Kumori 3
  mkdir -p ./cert/ca; rm -f ./cert/ca/ca.pem
  cat ./cert/ca1/ca.pem >> ./cert/ca/ca.pem
  cat ./cert/ca2/ca.pem >> ./cert/ca/ca.pem
  ;;

# Generate two client certs using CA1, one client cert using CA2, and
# one client cert using CA3
'generate-client-certs')
  generateClientCert myclientcert MyCompany 1 ${SERVICEURL} ./cert/ca1  ./cert/clientcerts
  generateClientCert myclientcert MyCompany 2 ${SERVICEURL} ./cert/ca1  ./cert/clientcerts
  generateClientCert myclientcert MyCompany 3 ${SERVICEURL} ./cert/ca2  ./cert/clientcerts
  generateClientCert myclientcert MyCompany 4 ${SERVICEURL} ./cert/ca3  ./cert/clientcerts
  ;;

# If we want to use an specific server-cert, instead of the default cluster cert
'generate-server-cert')
  mkdir -p ./cert/server
  openssl genrsa -out ./cert/server/servercert.key 2048
  openssl req -new \
    -key ./cert/server/servercert.key \
    -out ./cert/server/servercert.csr \
    -subj "/C=ES/O=Kumori/CN=${SERVICEURL}"
  openssl x509 -req \
    -days 30 \
    -in ./cert/server/servercert.csr \
    -CA ./cert/ca1/ca.pem \
    -CAkey ./cert/ca1/ca.key \
    -CAcreateserial \
    -out ./cert/server/servercert.pem
  ;;

# ------------------------------------------------------------------------------

'refresh-dependencies')
  cd manifests
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency --delete kumori.systems/builtins/inbound
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  ${KAM_CMD} mod dependency kumori.systems/builtins/inbound/@${KUMORI_INBOUND_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

# Only if the default cluster cert is not used
'create-custom-cert')
  ${KAM_CTL_CMD} register certificate ${CUSTOMSERVERCERT} \
    --domain ${SERVICEURL} \
    --cert-file ./cert/server/servercert.pem \
    --key-file ./cert/server/servercert.key \
    --public
  ;;

'create-ca')
  ${KAM_CTL_CMD} register ca ${CA} --ca-file ./cert/ca/ca.pem
  ;;

'create-domain')
  ${KAM_CTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

# Deploy the inbound using a self-signed cert
# ACCESING THIS INBOUND IS INSECURE, BECAUSE WE ARE USING A SERVER CERTIFICATE
# SIGNED USING A SELF-SIGNED CA
'deploy-inbound-custom')
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $CUSTOMSERVERCERT \
    --ca $CA \
    --client-cert
  ;;

# Deploy the inbound using the default cluster cert
'deploy-inbound')
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $DEFAULTCLUSTERCERT \
    --ca $CA \
    --client-cert
  ;;

# To check if our module is correct (from the point of view of the CUE syntax and
# the Kumori service model), we can use "kam process" to enerate the JSON
# representation
'dry-run')
  ${KAM_CMD} process deployment -t ./manifests
  ;;

'deploy-service')
  ${KAM_CMD} service deploy -d deployment -t ./manifests $DEPLOYNAME -- \
    --comment "Clientcert service" \
    --wait 5m
  ;;

# BE CAREFUL:
# Before use this command, you must adjust the "manifests/deployment-with-inbound/manifest.cue"
# file, because it contains resource names
'deploy-service-with-inbound')
  ${KAM_CMD} service deploy -d deployment-with-inbound -t ./manifests $DEPLOYNAME -- \
    --comment "Clientcert service" \
    --wait 5m
  ;;

'link')
  ${KAM_CTL_CMD} link $DEPLOYNAME:service $INBOUNDNAME:inbound
  ;;

'deploy-all')
  $0 create-ca
  $0 create-domain
  $0 deploy-inbound
  $0 deploy-service
  $0 link
  ;;

# BE CAREFUL:
# Before use this command, you must adjust the "manifests/deployment-with-inbound/manifest.cue"
# file, because it contains resource names
'deploy-all-with-inbound')
  $0 create-ca
  $0 create-domain
  $0 deploy-service-with-inbound
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  ;;

# ------------------------------------------------------------------------------

# Update deployment, so client-cert is required

'update-inbound')
  ${KAM_CTL_CMD} update inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $DEFAULTCLUSTERCERT \
    --ca $CA \
    --client-cert \
    --client-cert-required
  ;;

'update-service-with-inbound')
  ${KAM_CMD} service update -d deployment-with-inbound-cert-required -t ./manifests $DEPLOYNAME -- \
    --comment "Clientcert service with cert-required" \
    --wait 5m
  ;;

# ------------------------------------------------------------------------------

# Tests

# IF A SERVER-CERT CREATED USING A SELF-SIGNED CA IS USED, THEN ACCESING THE
# INBOUND IS INSECURE.
# SO: -k FLAG IS USED
# (the -k flag is not required if the default cluster cert is used)

# Expected: the "hello" service recognize the certificate of the company-1,
# created using the CA-1
'test-hello-1')
  echo
  echo Expected result: to recognize MyCompany1 certificate, using Kumori1 CA
  echo
  curl -k \
    --key  ./cert/clientcerts/myclientcert-1.key \
    --cert ./cert/clientcerts/myclientcert-1.pem \
    https://${SERVICEURL}/hello
  ;;

# Expected: the "hello" service recognize the certificate of the company-2,
# created using the CA-1
'test-hello-2')
  echo
  echo Expected result: to recognize MyCompany2 certificate, using Kumori1 CA
  echo
  curl -k \
    --key  ./cert/clientcerts/myclientcert-2.key \
    --cert ./cert/clientcerts/myclientcert-2.pem \
    https://${SERVICEURL}/hello
  ;;

# Expected: the "hello" service recognize the certificate of the company-3,
# created using the CA-2
'test-hello-3')
  echo
  echo Expected result: to recognize MyCompany3 certificate, using Kumori2 CA
  echo
  curl -k \
    --key  ./cert/clientcerts/myclientcert-3.key \
    --cert ./cert/clientcerts/myclientcert-3.pem \
    https://${SERVICEURL}/hello
  ;;

# Expected: the client certificate of the company-4 has been generated by a
# CA (CA-3) not included in the inbound, so an ssl error occurs
'test-hello-4')
  echo
  echo Expected result: ssl error because MyCompany4 certificate is not valid
  echo
  curl -k \
    --key  ./cert/clientcerts/myclientcert-4.key \
    --cert ./cert/clientcerts/myclientcert-4.pem \
    https://${SERVICEURL}/hello
  ;;

# Expected: the "hello" service doesnt recognize any certificate, but the anonymous
# request is processed in the "hello" service (if certRequired=false) or by
# the own inbound (if certRequired=true)
'test-hello-nocert')
  echo
  echo Expected result: no certificate found
  echo
  curl -k https://${SERVICEURL}/hello
  ;;

# All tests together
'test')
  echo
  echo Expected result: to recognize MyCompany1 certificate, using Kumori1 CA
  echo
  curl -k \
    --key  ./cert/clientcerts/myclientcert-1.key \
    --cert ./cert/clientcerts/myclientcert-1.pem \
    https://${SERVICEURL}/hello
  echo
  echo Expected result: to recognize MyCompany2 certificate, using Kumori1 CA
  echo
  curl -k \
    --key  ./cert/clientcerts/myclientcert-2.key \
    --cert ./cert/clientcerts/myclientcert-2.pem \
    https://${SERVICEURL}/hello
  echo
  echo Expected result: to recognize MyCompany3 certificate, using Kumori2 CA
  echo
  curl -k \
    --key  ./cert/clientcerts/myclientcert-3.key \
    --cert ./cert/clientcerts/myclientcert-3.pem \
    https://${SERVICEURL}/hello
  echo
  echo Expected result: ssl error because MyCompany4 certificate is not valid
  echo
  curl -k \
    --key  ./cert/clientcerts/myclientcert-4.key \
    --cert ./cert/clientcerts/myclientcert-4.pem \
    https://${SERVICEURL}/hello
  echo
  echo Expected result: no certificate found
  echo
  curl -k https://${SERVICEURL}/hello
  ;;

# ------------------------------------------------------------------------------

'unlink')
  ${KAM_CTL_CMD} unlink $DEPLOYNAME:service $INBOUNDNAME:inbound --wait 5m
  ;;

'undeploy-service')
  ${KAM_CMD} service undeploy $DEPLOYNAME -- --wait 5m --force
  ;;

'undeploy-inbound')
  ${KAM_CMD} service undeploy $INBOUNDNAME -- --wait 5m
  ;;

'delete-domain')
  ${KAM_CTL_CMD} unregister domain $DOMAIN --wait 5m
  ;;

'delete-ca')
  ${KAM_CTL_CMD} unregister ca ${CA} --wait 5m
  ;;

# NOTE: MAYBE YOU HAS REGISTER IT AS "ADMIN"
'delete-custom-cert')
  ${KAM_CTL_CMD} unregister certificate ${CUSTOMSERVERCERT} --wait 5m
  ;;

'undeploy-all')
  $0 unlink
  $0 undeploy-service
  $0 undeploy-inbound
  $0 delete-domain
  $0 delete-ca
  ;;

'undeploy-all-with-inbound')
  $0 undeploy-service
  $0 delete-domain
  $0 delete-ca
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac