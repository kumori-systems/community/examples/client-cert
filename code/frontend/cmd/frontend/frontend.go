package main

import (
	"fmt"
	"frontend/pkg/restapi"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

func main() {

	// Get configuration
	instanceID, err := getInstanceID()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}
	serverPort, err := getBindPortInt()
	if err != nil {
		fmt.Println("ERROR", err)
		os.Exit(1)
	}

	// Starts the http server
	restAPI := restapi.NewRestAPI(instanceID, serverPort)
	restAPI.Start()

	// Ctrl-c will abort the execution
	stopch := make(chan os.Signal)
	signal.Notify(stopch, os.Interrupt, syscall.SIGTERM)
	<-stopch
	os.Exit(0)
}

func getBindPortInt() (serverPort int, err error) {
	serverPortStr := os.Getenv("SERVER_PORT_ENV")
	serverPort, err = strconv.Atoi(serverPortStr)
	return
}

func getInstanceID() (instanceID string, err error) {
	instanceID = os.Getenv("KUMORI_INSTANCE_ID")
	if instanceID == "" {
		err = fmt.Errorf("InstanceID unknown")
	}
	return
}
