package restapi

import (
	"context"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type RestAPI struct {
	router     *mux.Router
	httpserver *http.Server
	bindPort   int
	InstanceID string
}

func NewRestAPI(
	instanceID string, bindPort int,
) (r *RestAPI) {
	router := mux.NewRouter()
	httpserver := &http.Server{
		Addr:    ":" + strconv.Itoa(bindPort),
		Handler: router,
	}
	r = &RestAPI{
		router:     router,
		httpserver: httpserver,
		bindPort:   bindPort,
		InstanceID: instanceID,
	}
	fmt.Println("NewRestAPI()")
	router.HandleFunc("/hello", r.GetHandlerHello).Methods("GET")
	router.HandleFunc("/health", r.GetHandlerHealth).Methods("GET")
	return
}

func (r *RestAPI) Start() {
	fmt.Println("RestAPI.Start()")
	go func() {
		r.httpserver.ListenAndServe()
	}()
}

func (r *RestAPI) Stop() {
	fmt.Println("RestAPI.Stop() Stopping")
	ctx, cancel := context.WithTimeout(
		context.Background(),
		time.Second*5,
	)
	defer cancel()
	r.httpserver.Shutdown(ctx)
	fmt.Println("RestAPI.Stop() Stopped")
}

func (r *RestAPI) GetHandlerHello(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.GetHandlerHello()")
	res.WriteHeader(http.StatusOK)
	result := "Hello from " + r.InstanceID
	certHeader, ok := req.Header["X-Forwarded-Client-Cert"]
	fmt.Printf("RestApi.GetHandlerHello() X-Forwarded-Client-Cert Header is %q\n", certHeader)
	if ok {
		result = result + ", and X-Forwarded-Client-Cert contains " + r.GetCertInfo(certHeader[0])
	} else {
		result = result + ", but X-Forwarded-Client-Cert not found"
	}
	fmt.Fprintln(res, result)
}

func (r *RestAPI) GetHandlerHealth(res http.ResponseWriter, req *http.Request) {
	fmt.Println("RestApi.GetHandlerHealth() returning OK")
	res.WriteHeader(http.StatusOK)
}

func (r *RestAPI) GetCertInfo(encodedCertPem string) string {
	decodedCertPem, err := url.QueryUnescape(encodedCertPem)
	if err != nil {
		return err.Error()
	}
	block, _ := pem.Decode([]byte(decodedCertPem))
	if block == nil || block.Type != "CERTIFICATE" {
		return "Failed to parse certificate PEM"
	}
	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return err.Error()
	}
	return fmt.Sprintf(
		"subject.CommonName:%v, subject.Organization:%v, subject.Country:%v, issuer.CommonName:%s, issuer.Organization:%v, issuer.Country:%v",
		cert.Subject.CommonName,
		cert.Subject.Organization,
		cert.Subject.Country,
		cert.Issuer.CommonName,
		cert.Issuer.Organization,
		cert.Issuer.Country,
	)
}
